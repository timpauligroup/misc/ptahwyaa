import abjad
from abjad import (BarLine, Clef, Dynamic, KeySignature, LilyPondFile,
                   MarginMarkup, Markup, MetronomeMark, NoteMaker, Rest, Score,
                   Staff, StaffGroup, TimeSignature, Voice, attach, inspect,
                   instruments, persist, setting)


def piano_range(pitches):
    return all([-49 <= p and p <= 49 for p in pitches])


def inters_to_pitches(start, inters):
    result = [start]
    for index, inter in enumerate(inters):
        current = start + sum(inters[:index]) + inter
        result.append(current)
    return result


def pitches_to_inters(pitches):
    result = []
    for curren, nex in zip(pitches, pitches[1:]):
        result.append(nex - curren)
    return result


def make_staff(notes):
    voice = Voice(notes)
    voice.remove_commands.append("Note_heads_engraver")
    voice.consists_commands.append("Completion_heads_engraver")
    voice.remove_commands.append("Rest_engraver")
    voice.consists_commands.append("Completion_rest_engraver")
    staff = Staff([voice])
    leaf = inspect(staff).leaf(0)
    instrument = instruments.Piano()
    attach(instrument, leaf)
    tempo = MetronomeMark((1, 4), 60)
    attach(tempo, leaf)
    clef = Clef('alto')
    attach(clef, leaf)
    time = TimeSignature((5, 4))
    attach(time, leaf)
    key = KeySignature(tonic='f', mode='major')
    attach(key, leaf)
    dynamic = Dynamic('p')
    attach(dynamic, leaf)
    bar_line = BarLine()
    return staff


# compose
def a(pitches, durations, y, z):
    reps1 = 4
    pitches += [0] * reps1
    durations += [20] * reps1
    reps2 = 2
    pitches += [y] * reps2 + [z] * reps2
    durations += [12, 8] * reps2
    return pitches, durations


def b(pitches, durations, y, z):
    pitches += [y] + [z]
    durations += [8, 12]
    return pitches, durations


lh_pitches = []
durations = []

lh_pitches, durations = a(lh_pitches, durations, 5, 2)
lh_pitches, durations = a(lh_pitches, durations, 2, 0)

lh_pitches, durations = b(lh_pitches, durations, -5, -7)
lh_pitches, durations = b(lh_pitches, durations, -10, -5)
lh_pitches, durations = b(lh_pitches, durations, -5, -7)
lh_pitches, durations = b(lh_pitches, durations, -5, -2)

lh_pitches, durations = a(lh_pitches, durations, -5, -2)

# end
lh_pitches += [0] * 4
durations += [20] * 4

# on a piano tuned in equal temperament notes from c which are near to integer
# fractions are d, f, g and b flat
assert(all([p % 12 in [0, 2, 5, 7, 10] for p in lh_pitches]))
assert(len(lh_pitches) == len(durations))

# second voice
lh_inters = pitches_to_inters(lh_pitches)
print("lh_inters", lh_inters)
assert(all([-5 <= p and p <= 5 for p in lh_inters]))
rh_inters = list(map(lambda x: x * x if x >= 0 else -(x * x), lh_inters))
print("sum", sum(rh_inters))
durations = list(map(lambda x: (x, 16), durations))

start = 0
rh_pitches = inters_to_pitches(start, rh_inters)

# score
print("lh_pitches", lh_pitches)
assert(piano_range(lh_pitches))
print("rh_pitches", rh_pitches)
assert(piano_range(rh_pitches))


maker = NoteMaker()
lh_notes = maker(lh_pitches, durations)
rh_notes = maker(list(reversed(rh_pitches)), list(reversed(durations)))
rh_notes = [Rest(rh.written_duration) if rh.written_pitch ==
            lh.written_pitch else rh for lh, rh in zip(lh_notes, rh_notes)]


# format
lh_staff = make_staff(lh_notes)
rh_staff = make_staff(rh_notes)

staff_group = StaffGroup([rh_staff, lh_staff], lilypond_type='PianoStaff')

setting(staff_group).instrument_name = Markup('Piano')

score = Score([staff_group])
score.add_final_bar_line()

# header
title = 'ptahwyaa'
lilypond_file = LilyPondFile.new(score)
lilypond_file.header_block.title = Markup(title)
lilypond_file.header_block.composer = Markup('Tim Pauli')
lilypond_file.header_block.meter = Markup('Play gently but like a machine.')
lilypond_file.header_block.tagline = Markup('github.com/inkeye/ptahwyaa')
lilypond_file.header_block.copyright = Markup('2017')
lilypond_file.paper_block.items.append('min-systems-per-page = #6')

# render
persist(lilypond_file).as_pdf(title + '.pdf')
persist(lilypond_file).as_midi(title + '.midi')
